package binario;
public class Binario {
    public void mostrarBinario(){
        ObtenerDatos ob=new ObtenerDatos();
        String binarioString;
        int binarioInt=0;
        String respuesta;
        int variableTemporal=0;
        int j=0;
        do{
        System.out.println("Escriba su numero en binario para convertirlo a decimal");
        binarioString=ob.leerDatos();
        for(int i=0;i<binarioString.length();i++){
            j++;
			try{
				variableTemporal=Integer.parseInt(binarioString.substring(i,j));
			}catch(NumberFormatException ioe){
				System.out.println("Esto no es un binario");
				mostrarBinario();
			}
            if(variableTemporal<2 && variableTemporal>-1){
                binarioInt=binarioInt*2+variableTemporal;
            }else{
                System.out.println("El numero que usted ha escrito no es binario");   
                mostrarBinario();
            }
        }
        System.out.println("El numero en decimal es: "+binarioInt);
        System.out.println("Desea hacerlo otra ves si o s/no o n");
        respuesta=ob.leerDatos();
        if(respuesta.equals("s") || respuesta.equals("si")){
            j=0;
        variableTemporal=0;
        binarioInt=0;
        }
        }while(respuesta.equals("s") || respuesta.equals("si"));
        if(respuesta.equals("n") || respuesta.equals("no") || respuesta!=("si") || respuesta!=("s")){
            System.out.println("Gracias por usar este programa");
            System.exit(1);
        }
    }
}

package binario;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class ObtenerDatos {
    BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
    String recolector;
    public String leerDatos(){
     try{
         recolector=br.readLine();
     }catch(IOException ioe){
         System.exit(1);
     }   
     return recolector;
    }
}
